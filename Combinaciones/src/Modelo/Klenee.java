package Modelo;

import java.util.LinkedList;

/**
 *
 * @author anderssonccg
 */
public class Klenee {
    
    private LinkedList<String> lang = new LinkedList();
    private int pow;

    public Klenee() {
        
    }
    
    public String getCombinations(int pow){
        if(pow < 0) throw new RuntimeException("Error. La potencia debe ser positiva o cero");
        this.pow = pow;
        this.lang.clear();
        this.lang.add("λ");
        this.createCombination("a", 0);
        this.createCombination("b", 0);
        return this.toString();
    }
    
    private void createCombination(String str, int i){
        if(i >= pow) return;
        lang.add(str);
        createCombination(str + "a", i+1);
        createCombination(str + "b", i+1);
    }

    public void setPow(int pow) {
        this.pow = pow;
    }

    public LinkedList<String> getLang() {
        return lang;
    }

    public int getPow() {
        return pow;
    }
    
    public String toString(){
        String str = "{ \n";
        for(String x : lang){
            str = str + " { " + x + " }, \n";
        }
        return str + "}";
    }
}
